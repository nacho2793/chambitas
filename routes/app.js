var express = require('express');
var router = express.Router();
var path = require('path');
var models = path.join(__dirname, '../models');
var User = require('../models/user');

/* GET home page. */
router.get('/index', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/', function(req, res, next) {

  res.render('../views/home');

});

router.get("/homeUsuarios",function(req, res){
  res.render('../views/homeUsuarios');
});

router.get("/homeWorkers",function(req, res){
  res.render('../views/homeUsuarios', req.session.user);
});

router.get("/chat",function(req, res){
  res.render('chat');
});


router.get("/home",function(req, res){
  res.render('home');
});

router.get("/changeProfile",function(req, res){
  res.render('editarPerfil');
  if(!req.session.user){
    res.redirect('../views/home');
  }

});

router.get("/iniciarSesion",function(req, res){
  res.render('iniciarSesion');
});
router.get("/guardados",function(req, res){
  res.render('guardados');
  if(!req.session.user){
    res.redirect('/home');
  }
});

router.get("/soy",function(req, res){
  res.render('soy');
});

router.get("/busco",function(req, res){
  res.render('busco');
});

router.get("/registro",function(req, res){
  res.render('../views/registro');
});

router.post("/busco", function(req, res, next){

  var wtf = {
    name:req.body.Nombre
  };

  user.save();
  res.redirect('/iniciarSesion');
});

module.exports = router;