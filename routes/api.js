var express = require('express');
var router = express.Router();
var app = express();
var path = require('path');
var models = path.join(__dirname, '../models');

/* GET home page. */

router.post("/registerWorker",function(req, res){

	var varUser = path.join(models, '/user');
	console.log(varUser);

	var User = require(varUser);
	var user = new User({
		first_name: req.body.first_name,
	    last_name: req.body.last_name, 
	    email: req.body.email,
	    password: req.body.password, 
	    type: req.body.type,
	    country: req.body.country,
	    city: req.body.city,
	    state: req.body.state
	    
	});

	user.save();
	res.json("OK");
});

router.route('/logIn').post(function(req, res){

	var varUser = path.join(models, '/user');
	var User = require(varUser);

	var query = User.findOne({'password' : req.body.password, 'email': req.body.email});
    query.exec(function(err, user){
    	if(err){
    	 console.log(err)
    	 return handleError(err); 
    	}

    	req.session.user = user;
    	
    })
});

router.route('/logOut').post(function(req, res){

	var varUser = path.join(models, '/user');
	var User = require(varUser);
    	
    	req.session.user = (false);

    	console.log('Session Terminated');
});

router.post("/updateProfile",function(req,res){

});

router.post("/setProfileWorker",function(req,res){
	
	var diagSetProfile = path.join(models, '/setProfileWorker');
	console.log(diagSetProfile);
 
	var Profile = require(diagSetProfile);
	var profile = new Profile({
		rate: req.body.rate,
		profession: req.body.profession,
		services: req.body.services
	});

	user.save();
	res.json("OK");
});


router.post("/typeGetSession",function(req,res){

});

module.exports = router;