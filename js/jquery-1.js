var pokemons = [
	{
		"number":1,
		"name":"bulbasaur",
		"type":"grass"
	},
	{
		"number":4,
		"name":"charmander",
		"type":"fire"
	},
	{
		"number":7,
		"name":"squirtle",
		"type":"water"
	}
];


$("#test-button").click(function(){
	for(var i = 0; i < pokemons.length; i++){
		var poke = pokemons[i];
		$("#pokemons").append("<button>" + poke.name + "-" + poke.type + "</button>");		
		$("#pokemons").append("<br>"); 
		console.log(poke);
	}
});

